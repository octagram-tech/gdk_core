pub fn split_string(delim: String, value: &String) -> Vec<&str> {
    value.split(&delim[..]).collect()
}

pub fn convert_slices_to_strings(slices: &Vec<&str>) -> Vec<String> {
    let mut strings: Vec<String> = Vec::new();
    slices.iter().for_each(|slice| strings.push(slice.to_string()));
    strings
}

pub fn string_to_u128(value: &String) -> u128 {
    value.parse::<u128>().unwrap()
}

pub fn set_string(string: &mut String, slice: &str) {
    string.clear();
    string.shrink_to_fit();
    string.push_str(&slice[..]);
}
