pub mod helpers;

pub mod prelude {
    use std::{any::Any, cell::RefCell, fmt, rc::Rc};

    pub trait IAny: Any {
        fn as_any(&mut self) -> &mut dyn Any;
    }

    pub trait UpcastFrom<T: ?Sized> {
        fn up_from(value: &T) -> &Self;
        fn up_from_mut(value: &mut T) -> &mut Self;
    }
    pub trait Upcast<U: ?Sized> {
        fn up(&self) -> &U;
        fn up_mut(&mut self) -> &mut U;
    }

    impl<T: ?Sized, U: ?Sized> Upcast<U> for T
    where
        U: UpcastFrom<T>,
    {
        fn up(&self) -> &U {
            U::up_from(self)
        }
        fn up_mut(&mut self) -> &mut U {
            U::up_from_mut(self)
        }
    }

    pub trait IRenderer {
        fn update(&mut self) {}
        fn draw(&mut self) {}
    }

    pub trait IState {
        fn init(&mut self) {}
        fn exit(&mut self) {}
        fn run(&mut self) {}
        fn is_active(&self) -> bool {
            true
        }
    }

    pub struct GameEventNode<TEvents> {
        pub repeat: bool,
        pub complete: bool,
        pub ready: bool,
        pub parent_id: String,
        pub next_events: Vec<Rc<RefCell<Box<dyn IGameEvent<TEvents>>>>>,
        pub next_event_ids: Vec<String>,
    }

    impl<TEvents> Default for GameEventNode<TEvents> {
        fn default() -> Self {
            GameEventNode {
                ready: false,
                repeat: false,
                complete: false,
                parent_id: String::default(),
                next_event_ids: Vec::default(),
                next_events: Vec::default(),
            }
        }
    }

    pub trait IGameEvent<TEvents> {
        fn get_id(&self) -> &String;
        fn get_type(&self) -> TEvents;
        fn get_parent_event_id(&self) -> &String;
        fn get_next_events_mut(&mut self) -> &mut Vec<Rc<RefCell<Box<dyn IGameEvent<TEvents>>>>>;
        fn is_ready(&self) -> bool;
        fn is_repeat(&self) -> bool;
        fn is_complete(&self) -> bool;
        fn set_ready(&mut self, value: bool);
        fn set_complete(&mut self, value: bool);
        fn set_parent_event_id(&mut self, value: String);
        fn stop(&mut self);
        fn reset(&mut self);
        fn start(&mut self);
        fn update(&mut self);
        fn add_next_event(&mut self, value: Rc<RefCell<Box<dyn IGameEvent<TEvents>>>>);
    }

    impl<TEvents> fmt::Debug for dyn IGameEvent<TEvents> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "")
        }
    }

    pub mod time {
        use std::collections::hash_map::HashMap;
        use std::time::Instant;

        static mut TIMERS: Option<HashMap<String, Timer>> = None;

        unsafe fn get_timers() -> &'static mut HashMap<String, Timer> {
            if TIMERS.is_none() {
                return TIMERS.get_or_insert(HashMap::new());
            } else {
                return TIMERS.as_mut().unwrap();
            }
        }

        pub fn new<'a>(id: &String, timer: Timer) {
            unsafe { get_timers().insert(id.clone(), timer) };
        }

        pub fn get(id: &String) -> Option<&'static mut Timer> {
            unsafe { get_timers().get_mut(id) }
        }

        pub fn update() {
            let timers = unsafe { get_timers() };
            if timers.len() > 0 {
                timers.retain(|_, timer| {
                    if !timer.is_finished() {
                        return true;
                    } else {
                        if !timer.is_kill_on_complete() {
                            return true;
                        }
                    }
                    return false;
                });
                timers.iter_mut().for_each(|(_, timer)| timer.update());
            }
        }

        pub struct Timer {
            finished: bool,
            active: bool,
            duration: u128,
            kill_on_complete: bool,
            time: Option<Instant>,
        }

        impl Timer {
            pub fn new(duration: u128, auto_start: bool, kill_on_complete: bool) -> Self {
                let mut timer = Timer {
                    duration,
                    kill_on_complete,
                    finished: false,
                    active: false,
                    time: None,
                };

                if auto_start {
                    timer.start();
                }

                timer
            }

            pub fn is_active(&self) -> bool {
                self.active
            }

            pub fn is_kill_on_complete(&self) -> bool {
                self.kill_on_complete
            }

            pub fn is_finished(&self) -> bool {
                self.finished
            }

            pub fn set_duration(&mut self, value: u128) {
                self.duration = value;
            }

            pub fn start(&mut self) {
                self.active = true;
                self.time = Some(Instant::now());
            }

            pub fn stop(&mut self) {
                self.time = None;
                self.active = false;
                self.finished = false;
            }

            pub fn reset(&mut self) {
                self.stop();
            }

            pub fn update(&mut self) {
                if self.time.is_some() {
                    let instant = self.time.unwrap();
                    let current = instant.elapsed().as_millis();
                    if current >= self.duration {
                        self.time = None;
                        self.finished = true;
                        self.active = false;
                    }
                }
            }
        }
    }
}
